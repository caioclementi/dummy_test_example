import unittest
from game import myGame

class TestDumy(unittest.TestCase):

    def test_dummy_return(self):
        self.assertEqual(myGame(), 'This is a dummy return')

if __name__ == '__main__':
    unittest.main()